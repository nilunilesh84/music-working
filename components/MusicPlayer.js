import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  Text,
  SafeAreaView,
  Dimensions,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Slider from '@react-native-community/slider';
import songs from '../model/data';
import Sound from 'react-native-sound'


const { width, height } = Dimensions.get('window');


export default function MusicPlayer() {
  const [songIndex, setsongIndex] = useState(0);
  const [play, setplay] = useState(false)
  var sound1 = new Sound(songs[songIndex].music,
    (error, sound) => {
      if (error) {
        alert('error' + error.message);
        return;
      }
    });
  const songsSlider = useRef(null);
  const scrollX = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    scrollX.addListener(({ value }) => {
      const index = Math.round(value / width);
      setsongIndex(index);
    });
    return () => {
      scrollX.removeAllListeners();
    };
  }, []);

  const playSong = () => {
    setplay(true)
    console.log('played')
    sound1.play();
  }
  const pauseSong = () => {
    setplay(false)
    console.log('pause')
    sound1.stop()
  }
  const toggleSong = () => {
    if (play) {
     return pauseSong()
    }else {
     return playSong()
    }
  }
  const skiptoNext = () => {
    setsongIndex(songIndex + 1)
    songsSlider.current.scrollToOffset({
      offset: (songIndex + 1) * width,
    });
  };
  const skiptoPrevious = () => {
    setsongIndex(songIndex - 1)
    songsSlider.current.scrollToOffset({
      offset: (songIndex - 1) * width,
    });
  };
  const renderSongs = ({ item, index }) => {
    return (
      <Animated.View
        style={{ width: width, justifyContent: 'center', alignItems: 'center' }}>
        <View style={styles.imageWrapper}>
          <Image style={styles.image} source={item.image} />
        </View>
      </Animated.View>
    );
  };
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.maincontainer}>
        <View style={{ width: width }}>
          <Animated.FlatList
            ref={songsSlider}
            data={songs}
            renderItem={renderSongs}
            keyExtractor={item => item.id}
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={6}
            onScroll={Animated.event(
              [
                {
                  nativeEvent: {
                    contentOffset: { x: scrollX },
                  },
                },
              ],
              { useNativeDriver: true },
            )}
          />
        </View>
        <View>
          <Text style={styles.songtitle}>{songs[songIndex].title}</Text>
          <Text style={styles.songartist}>{songs[songIndex].artist}</Text>
        </View>
        <View>
          <Slider
            style={styles.Slider}
            value={10}
            minimumValue={0}
            maximumValue={100}
            thumbTintColor="#FFD369"
            minimumTrackTintColor="#FFD369"
            maximumTrackTintColor="white"
          // onSlidingComplete={() => { }}
          />
        </View>
        <View style={styles.timerContainer}>
          <Text style={styles.timer}>0:00</Text>
          <Text style={styles.timer}>5:00</Text>
        </View>
        <View style={styles.musiccontrol}>
          <TouchableOpacity
            onPress={skiptoPrevious}
          >
            <Ionicons
              name="play-skip-back-outline"
              size={35}
              color="#FFD369"
              style={{ marginTop: 16 }}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            toggleSong()
          }}>
            <Ionicons
              name={play ? 'ios-pause-circle' :  'ios-play-circle'}
              size={60}
              color="#FFD369"
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={skiptoNext}>
            <Ionicons
              name="play-skip-forward-outline"
              size={35}
              color="#FFD369"
              style={{ marginTop: 16 }}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.bottomContainer}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            width: '80%',
          }}>
          <TouchableOpacity>
            <Ionicons name="heart-outline" size={30} color="#777777" />
          </TouchableOpacity>
          <TouchableOpacity>
            <Ionicons name="repeat" size={30} color="#777777" />
          </TouchableOpacity>
          <TouchableOpacity>
            <Ionicons name="share-outline" size={30} color="#777777" />
          </TouchableOpacity>
          <TouchableOpacity>
            <Ionicons name="ellipsis-horizontal" size={30} color="#777777" />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#222831',
  },
  maincontainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageWrapper: {
    width: 300,
    height: 340,
    marginBottom: 25,
  },
  image: {
    width: ' 100%',
    height: '100%',
    borderRadius: 15,
  },
  bottomContainer: {
    borderTopColor: 'white',
    borderTopWidth: 2,
    width: width,
    alignItems: 'center',
    paddingVertical: 15,
  },
  songtitle: {
    fontSize: 22,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#EEEEEE',
  },
  songartist: {
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#EEEEEE',
  },
  Slider: {
    width: 370,
    height: 40,
    marginTop: 20,
    flexDirection: 'row',
  },
  timerContainer: {
    width: 320,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  timer: {
    color: 'white',
  },
  musiccontrol: {
    width: '60%',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
