import React from 'react';
import MusicPlayer from './components/MusicPlayer';
import { View,StyleSheet,StatusBar,Text} from 'react-native';


export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar barStyle='light-content'/>
        <MusicPlayer/>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
