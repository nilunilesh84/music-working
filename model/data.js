const songs = [
  {
    title: 'Manohari',
    artist: 'Revanth',
    id: '1',
    image: require('../assets/manohari.jpg'),
    music: require('../songs/Manoha.mp3'),
  },
  {
    title: 'Sahore Bahubali',
    artist: 'Daler Mehndi',
    id: '2',
    image: require('../assets/sahore.jpg'),
    music: require('../songs/Jiyo.mp3'),
  },
  {
    title: 'Dandaalayyaa',
    artist: 'Kaala Bhairava',
    id: '3',
    image: require('../assets/bahu.jpeg'),
    music: require('../songs/Danda.mp3'),
  },
];

export default songs;
